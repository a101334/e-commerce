import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { RegisterProductsComponent } from './views/register-products/register-products.component';
import { HomeComponent } from './views/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { AdminPanelComponent } from './views/admin-panel/admin-panel.component';
import { ProductsManagementComponent } from './views/products-management/products-management.component';
import { MenuAdminComponent } from './components/menu-admin/menu-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterProductsComponent,
    HomeComponent,
    FooterComponent,
    AdminPanelComponent,
    ProductsManagementComponent,
    MenuAdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
