import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  registerProduct(newProductObj: any) {
    console.log(newProductObj);
  }

  constructor(
    private http: HttpClient
  ) { }
}
