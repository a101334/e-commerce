import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPanelComponent } from './views/admin-panel/admin-panel.component';
import { RegisterProductsComponent } from './views/register-products/register-products.component';
import { HomeComponent } from './views/home/home.component';
import { ProductsManagementComponent } from './views/products-management/products-management.component';



const routes: Routes = [
  { path: '', redirectTo: 'admin-panel/register-products', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'admin-panel/register-products', component: RegisterProductsComponent },
  { path: 'admin-panel/products-management', component: ProductsManagementComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
