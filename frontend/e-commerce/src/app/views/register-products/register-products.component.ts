import { Component, OnInit } from '@angular/core';

import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-register-products',
  templateUrl: './register-products.component.html',
  styleUrls: ['./register-products.component.scss']
})
export class RegisterProductsComponent implements OnInit {
  isValidForm: boolean;

  register() {
    this.isValidForm = true;

    const inputs = document.querySelectorAll('input[type=text]');
    const inputFile = (<HTMLInputElement>document.getElementById('img-product')).files;
    let newProduct = {};

    inputs.forEach((item) => {
      const key = (<HTMLInputElement>item).name;
      const value = (<HTMLInputElement>item).value;
      newProduct[key] = value;
    })

    for(const key in newProduct) {
      if(!newProduct[key]) {
        this.isValidForm = false;
      }
    }

    newProduct['imgFile'] = inputFile;

    if(newProduct['imgFile'].length != 1 ) {
      this.isValidForm = false;
    }

    if(this.isValidForm) {
      this.productsService.registerProduct(newProduct);
    } else {
      alert('Invalid Form, fill all fields, please.');
    }

  }
  
  constructor(
    private productsService: ProductsService
  ) { }

  ngOnInit() {
    document.querySelector('button').addEventListener('click', () => {
      this.register();
    })
  }

}
